import static org.junit.Assert.*;

import org.junit.After;


/**
 * Author: Mohamed Sayed
 * Date: 26th of April 2018
 * Purpose: The Test class has a test method which is divided into 3 parts, each for a type of student
 * 			Every operation is performed on each type of student
 * 			Boundary values are tested too
 * 			checks for nulls
 *
 */
class Test {
	
	int i = 0;
	UndergraduateStudent mo = new UndergraduateStudent("mo", "sayed", 29, 8, 1999);
	PostgraduateTaught pt = new PostgraduateTaught("Ross", "Doddss", 29, 8, 1995);
	PostgraduateResearch f = new PostgraduateResearch("Joe", "Ayton", 29, 8, 1996);

	
	
	@org.junit.jupiter.api.Test
	public void testUndergraduateStudent() {
		
			
			mo.registerStudent(mo);
			assertEquals("mo", mo.getFirstName());
			assertEquals("sayed", mo.getLastName());
			assertEquals(18, mo.getAge());
			assertEquals("MS-2018-00", mo.getSmartCard().getSMART_CARD_NO());
			assertEquals("a0000", mo.getStudentID().getSTUDENT_ID());
			
			mo.amendStudentData("a0000", "mo", "salah", "15", "6", "1992");
			
			assertEquals("mo", mo.getFirstName());
			assertEquals("salah", mo.getLastName());
			assertEquals(25, mo.getAge());
			assertEquals("MS-2018-00", mo.getSmartCard().getSMART_CARD_NO());
			assertEquals("a0000", mo.getStudentID().getSTUDENT_ID());
			
			
			
			try {
				mo.amendStudentData("a0001", "Bob", "Stone", "1", "1", "1330");
			}
			catch(Exception e) {
				System.err.println("You are more than a 100 years old -- " + e.getMessage());
			}
			
			try {
				mo.amendStudentData("a0001", "", "", "11", "19", "1997");
			}
			catch(Exception e) {
				System.err.println("Name is invalid -- " + e.getMessage());
			}
			
			try {
				mo.amendStudentData("a0001", null, null, "11", "19", "1997");
			}
			catch(Exception e) {
				System.err.println("Name is invalid -- " + e.getMessage());
			}
			
			try {
				mo.amendStudentData("z9873", "Max", "Jack", "11", "19", "1997");
			}
			catch(Exception e) {
				System.err.println("Student ID does not exist -- " + e.getMessage());
			}
			
			try {
				mo.terminateStudent("a0000");
				assertEquals("mo", mo.getFirstName());

			}
			
			catch(NullPointerException e) {
				System.err.println("Student has been deleted");
			}
			
			assertEquals(0, AbstractStudent.noOfStudents("Undergraduate Student"));
			
			
/**
 * *********************************************************************************************************************			
 */
			
			pt.registerStudent(pt);
			try {
				pt.registerStudent(pt);
			}
			catch(Exception e) {
				System.err.println(e.getMessage());
			}
			assertEquals("Ross", pt.getFirstName());
			assertEquals("Doddss", pt.getLastName());
			assertEquals(22, pt.getAge());
			assertEquals("RD-2018-00", pt.getSmartCard().getSMART_CARD_NO());
			assertEquals("a0001", pt.getStudentID().getSTUDENT_ID());
			
			try {
				pt.amendStudentData("a0001", "Bob", "Stone", "1", "1", "1330");
			}
			catch(Exception e) {
				System.err.println("You are more than a 100 years old -- " + e.getMessage());
			}
			
			try {
				pt.amendStudentData("a0001", "", "", "11", "19", "1997");
			}
			catch(Exception e) {
				System.err.println("Name is invalid -- " + e.getMessage());
			}
			
			try {
				pt.amendStudentData("a0001", null, null, "11", "19", "1997");
			}
			catch(Exception e) {
				System.err.println("Name is invalid -- " + e.getMessage());
			}
			
			try {
				pt.amendStudentData("z9873", "Max", "Jack", "11", "19", "1997");
			}
			catch(Exception e) {
				System.err.println("Student ID does not exist -- " + e.getMessage());
			}
			
			
			pt.amendStudentData("a0001", "Bob", "Stone", "27", "7", "1996");
			
			assertEquals("Bob", pt.getFirstName());
			assertEquals("Stone", pt.getLastName());
			assertEquals(21, pt.getAge());
			assertEquals("BS-2018-00", pt.getSmartCard().getSMART_CARD_NO());
			assertEquals("a0001", pt.getStudentID().getSTUDENT_ID());
			
			try {
				pt.terminateStudent("a0001");
				assertEquals("Ross", mo.getFirstName());

			}
			
			catch(NullPointerException e) {
				System.err.println("Student has been deleted");
			}
			
			assertEquals(0, AbstractStudent.noOfStudents("postgraduate taught"));
			

/**
 * *********************************************************************************************************************
 */
			
			
			f.registerStudent(f);
			
			assertEquals("Joe", f.getFirstName());
			assertEquals("Ayton", f.getLastName());
			assertEquals(21, f.getAge());
			assertEquals("JA-2018-00", f.getSmartCard().getSMART_CARD_NO());
			assertEquals("a0002", f.getStudentID().getSTUDENT_ID());
			
			
			f.amendStudentData("a0002", "James", "Carl", "19", "7", "1994");
			
			assertEquals("James", f.getFirstName());
			assertEquals("Carl", f.getLastName());
			assertEquals(23, f.getAge());
			assertEquals("JC-2018-00", f.getSmartCard().getSMART_CARD_NO());
			assertEquals("a0002", f.getStudentID().getSTUDENT_ID());
			assertEquals(1, AbstractStudent.noOfStudents("postgraduate research"));
			
			try {
			f.registerStudent(f);
			} 
			catch(Exception e) {
				System.err.println("Exception caught because student cannot be "
						+ "registered twice");
			}
			
			try {
				f.amendStudentData("a0001", null, null, "11", "19", "1997");
			}
			catch(Exception e) {
				System.err.println("Name is invalid -- " + e.getMessage());
			}
			
			try {
				f.amendStudentData("a0001", "Bob", "Stone", "1", "1", "1330");
			}
			catch(Exception e) {
				System.err.println("You are more than a 100 years old -- " + e.getMessage());
			}
			
			try {
				f.amendStudentData("a0001", "", "", "11", "19", "1997");
			}
			catch(Exception e) {
				System.err.println("Name is invalid -- " + e.getMessage());
			}
			
			try {
				f.amendStudentData("a0001", null, null, "11", "19", "1997");
			}
			catch(Exception e) {
				System.err.println("Name is invalid -- " + e.getMessage());
			}
			
			try {
				f.amendStudentData("z9873", "Max", "Jack", "11", "19", "1997");
			}
			catch(Exception e) {
				System.err.println("Student ID does not exist -- " + e.getMessage());
			}
			
			try {
				f.terminateStudent("a0002");
				assertEquals("mo", mo.getFirstName());

			}
			
			catch(NullPointerException e) {
				System.err.println("Student has been deleted");
			}
			
		assertEquals(0, AbstractStudent.noOfStudents("postgraduate research"));
		i++;
	}


	@After
	public void testNoOfStudents() {
		
		int ugs = AbstractStudent.noOfStudents("Undergraduate Student");
		
		assertEquals(i,ugs);
		
		int pt = AbstractStudent.noOfStudents("postgraduate taught");
		
		assertEquals(i,pt);
		
		int pr = AbstractStudent.noOfStudents("postgraduate research");
		
		assertEquals(i,pr);
		
	}
		
}
