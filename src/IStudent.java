import java.time.LocalDate;
import java.util.Date;
import java.util.List;

/**
 * Author: Mohamed Sayed
 * Date: 26th of April 2018
 * Purpose: The IStudent interface contains all the methods shared between any Student class
 * 			according to the University's specifications
 *
 */

public interface IStudent {
	
	public void registerStudent(IStudent student);
	
	public void amendStudentData(String studentID, String firstName,
			String lastName, String dayob, String monthob, String yearob);
	
	public void terminateStudent(String studentID);
	
	public StudentID getStudentID();
	
	public SmartCard getSmartCard();
	
	public String getFirstName();
	
	public String getLastName();
	
	public int getAge();
	
	public LocalDate getLocalDateOfBirth();
	
	public Date getDateOfBirth();

}
