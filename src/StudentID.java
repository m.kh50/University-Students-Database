import java.util.HashMap;
import java.util.Map;


public final class StudentID {
	
	/**
	 * Author: Mohamed Sayed
	 * Date: 26th of April 2018
	 * Purpose: Allocates a unique student ID for a Student Object
	 * studentID is the variable that keeps on updating itself till 
	 * 		it becomes unique
	 * STUDENT_ID is the actual constant variable that then stores
	 * 		the unique student ID
	 *
	 */
	private String studentID;
	private final String STUDENT_ID;
	private final char[] ALPHABETS = "abcdefghijklmnopqrstuvwxyz".toCharArray();
	private static int alphaIndex = 0;
	private static int serialNumber = 0;
	
	/**
	 * The map keeps track of unique Student IDs and stores
	 * all the student objects along with their ID
	 */
	public static Map<String, IStudent> map = new HashMap<>();
	
	/**
	 * StudentID constructor 
	 * @param student takes in any Student Object
	 * Returns a unique Student ID 
	 */
	 <T extends IStudent> StudentID(T student) {
		do {
				studentID = "";
				studentID += ALPHABETS[alphaIndex];
				
				if(serialNumber < 10)
					studentID += "000" + serialNumber;
				
				if(serialNumber < 100 && serialNumber >= 10)
					studentID += "00" + serialNumber;
				
				if(serialNumber < 1000 && serialNumber >= 100)
					studentID += "0" + serialNumber;
				
				serialNumber++;
				
				if(serialNumber == 10000) {
					alphaIndex++;
					serialNumber = 0;
					if(alphaIndex == ALPHABETS.length)
						alphaIndex = 0;
				}
		} 
		while(map.containsKey(studentID)); 
				
		if(!map.containsKey(studentID)) {
				STUDENT_ID = new String(studentID);
				map.put(STUDENT_ID, student);
		}
			
			else throw new IllegalArgumentException("Student already exists");
	}
	
	/**
	 * @return - Getter method of map object
	 */
	static final Map<String, IStudent> getMap() {
		return map;
	}
	
	/**
	 * removes student object and student ID 
	 * in case a student is being terminated
	 * @param studentID
	 */
	static final void removeStudent(String studentID) {
		map.remove(studentID);
	}
	
	/**
	 * Returns student ID of student object
	 * @return
	 */
	public final String getSTUDENT_ID() {
		return STUDENT_ID;
	}
	
	/**
	 * Overrides toString method, presentes Student ID 
	 * in an elegant and a clear form
	 */
	public final String toString() {
		return String.format("Student ID: %s", STUDENT_ID);
	}

	/**
	 * hashCode method overridden
	 */
	@Override
	public final int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((STUDENT_ID == null) ? 0 : STUDENT_ID.hashCode());
		return result;
	}
	
	/**
	 * equals method overridden
	 */
	@Override
	public final boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof StudentID)) {
			return false;
		}
		StudentID other = (StudentID) obj;
		if (STUDENT_ID == null) {
			if (other.STUDENT_ID != null) {
				return false;
			}
		} else if (!STUDENT_ID.equals(other.STUDENT_ID)) {
			return false;
		}
		return true;
	}
	
	

}
