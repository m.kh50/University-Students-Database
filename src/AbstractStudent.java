import java.time.LocalDate;
import java.time.Period;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.List;

/**
 * Author: Mohamed Sayed
 * Date: 26th of April 2018
 * Purpose: The AbstractStudent class has a partial implementation of the three student classes
 * 			It fully implements all the methods inherited from the IStudent interface as they are all shared
 * 				in the subclasses
 * 			Specialized methods will be then implemented in the subclasses 
 *
 */

public abstract class AbstractStudent implements IStudent {
	
	/**
	 * Variables to be used to define a general Student object
	 * All variables are private and final (Defensive Programming) prevent
	 * 	having any student's object state from being changed after being initialized
	 */
	private Name name;
	private int age;
	private int dayob;
	private int monthob;
	private int yearob;
	private Date dateOfBirth;
	private LocalDate localDateOfBirth;
	private SmartCard smartCard;
	private StudentID studentID;
	
	/**
	 * This class also carries out the functionality of a Student class factory
	 * it will keep track of Student objects created
	 * checks if all Student objects are unique
	 * Stores each Student Object created in the map that matches its type
	 * along with the StudentID (String) form
	 */
	private static Map<String, UndergraduateStudent> undergraduates = new HashMap();
	private static Map<String, PostgraduateTaught> postgraduateTaughts = new HashMap();
	private static Map <String, PostgraduateResearch> postgraduateResearchs = new HashMap();
	
	/**
	 * General Student constructor
	 * @param firstName stores first name of student
	 * @param lastName stores last name of student 
	 * @param dayob stores day of birth of student
	 * @param monthob stores month of birth of student
	 * @param yearob stores year of birth of student
	 * 
	 * Checks if all the parameters have valid values
	 */
	AbstractStudent(String firstName, String lastName, int dayob, int monthob, int yearob) {
		
		if(firstName.length() > 0 && firstName != null && lastName != null
				&& lastName.length() > 0 && dayob > 0 && monthob > 0 && yearob > 1950 &&
				dayob <= 31 && monthob < 13 && yearob <= 2000) {
			this.name = new Name(firstName, lastName);
			this.dayob = dayob;
			this.monthob = monthob;
			this.yearob = yearob;
			Calendar cal = Calendar.getInstance();
			cal.set(yearob, monthob-1, dayob);
			this.dateOfBirth = cal.getTime();
			this.localDateOfBirth = LocalDate.of(yearob, monthob, dayob);
			LocalDate now = LocalDate.now();
			this.age = Period.between(localDateOfBirth, now).getYears();
			this.smartCard = null;
			this.studentID = null;
		}
		else throw new IllegalArgumentException("Student's data is invalid");
	}
	
	/**
	 * Give it type of students
	 * @param typeOfStudent
	 * Returns number of students of the same @param typeOfStudent
	 */
	public static final int noOfStudents(String typeOfStudent) {
		
		if(typeOfStudent.equalsIgnoreCase("Undergraduate Student")) return undergraduates.size();
		if(typeOfStudent.equalsIgnoreCase("postgraduate taught")) return postgraduateTaughts.size();
		if(typeOfStudent.equalsIgnoreCase("postgraduate research")) return postgraduateResearchs.size();
		
		return -1;

	}
	
	/**
	 * Registers student
	 * @param student - takes in a student in its parameters
	 * Uses Student's details for recording him/her 
	 * Allocates a studentID
	 * Allocates a smart card
	 */
	public final void registerStudent(IStudent student) {
		
		if(student instanceof UndergraduateStudent) {
			// create student object
			// check if he is at least 17
			// allocate a student id and smart card
			// add to maps
			UndergraduateStudent us = 
					(UndergraduateStudent) student;
			if(!undergraduates.containsValue(us)) {
				smartCard = new SmartCard(us);
				studentID = new StudentID(us);
				undergraduates.put(studentID.getSTUDENT_ID(), us);
			}
			else throw new IllegalArgumentException("Student already exists");
		
		}
		
		else if(student instanceof PostgraduateTaught) {
			PostgraduateTaught pt = 
					(PostgraduateTaught) student;
			if(!postgraduateTaughts.containsValue(pt)) {
				smartCard = new SmartCard(pt);
				studentID = new StudentID(pt);
				postgraduateTaughts.put(studentID.getSTUDENT_ID(), pt);
			}
			else throw new IllegalArgumentException("Student Already exists");
		
		}
		
		else if(student instanceof PostgraduateResearch) {
			PostgraduateResearch pr = 
					(PostgraduateResearch) student;
			if(!postgraduateResearchs.containsValue(pr)) {
				smartCard = new SmartCard(pr);
				studentID = new StudentID(pr);
				postgraduateResearchs.put(studentID.getSTUDENT_ID(), pr);
			}
			else throw new IllegalArgumentException("Student Already exists");
		
		}
		else throw new IllegalArgumentException("Type of student is invalid");
	}
	
	
	/**
	 * Edits student data using the student's id
	 * adds it back to the records again after being updated
	 */
	public final void amendStudentData(String studentID, String firstName,
			String lastName, String dayob, String monthob, String yearob) {
			
		if(firstName.length() > 0 && firstName != null && lastName != null
				&& lastName.length() > 0 && Integer.valueOf(dayob) > 0 && Integer.valueOf(monthob) > 0
				&& Integer.valueOf(yearob) > 1950 && Integer.valueOf(dayob) <= 31 
				&& Integer.valueOf(monthob) < 13 && Integer.valueOf(yearob) <= 2000) {
			
			if(undergraduates.containsKey(studentID)) {
				AbstractStudent s = undergraduates.get(studentID);
				
				s.setName(firstName, lastName);
				
				s.setDateOfBirth(Integer.valueOf(dayob), 
						Integer.valueOf(monthob), Integer.valueOf(yearob));
				
				smartCard.removeStudent();
				
				smartCard = new SmartCard(s);
				
			}
			
			else if(postgraduateTaughts.containsKey(studentID)) {
				AbstractStudent s = postgraduateTaughts.get(studentID);
				
				s.setName(firstName, lastName);
				
				s.setDateOfBirth(Integer.valueOf(dayob), 
						Integer.valueOf(monthob), Integer.valueOf(yearob));
				
				smartCard.removeStudent();
				
				smartCard = new SmartCard(s);
			}
			
			else if(postgraduateResearchs.containsKey(studentID)) {
				AbstractStudent s = postgraduateResearchs.get(studentID);
				
				s.setName(firstName, lastName);
				
				s.setDateOfBirth(Integer.valueOf(dayob), 
						Integer.valueOf(monthob), Integer.valueOf(yearob));
				
				smartCard.removeStudent();
				
				smartCard = new SmartCard(s);
			}
			
			else throw new IllegalArgumentException("Student does not exist");
		}
		else throw new 
			IllegalArgumentException("Invalid details");
	}
	
	
	
	/**
	 * Deletes everything related to the student object
	 * Deletes student ID
	 * Deletes Smart Card
	 * Deletes the student details
	 */
	public final void terminateStudent(String studentID) {
		//find student in map 
		//remove from smartcard map
		//remove from studentid map
		//remove from the map of its class 
		if(undergraduates.containsKey(studentID)) {
			undergraduates.remove(studentID);
			smartCard.removeStudent();
		}
		
		else if(postgraduateTaughts.containsKey(studentID)) {
			postgraduateTaughts.remove(studentID);
			smartCard.removeStudent();
		}
		
		else if(postgraduateResearchs.containsKey(studentID)) {
			postgraduateResearchs.remove(studentID);
			smartCard.removeStudent();
		}
		
		else throw new IllegalArgumentException("Student does not exist");
		StudentID.removeStudent(studentID);
		
		name = null;
		dayob = 0;
		monthob = 0;
		yearob = 0;
		smartCard = null;
		studentID = null;		
		
	}
	
	private final void setName(String firstName, String lastName) {
		this.name = new Name(firstName, lastName);
	}
	
	private final void setDateOfBirth(int dayob, int monthob, int yearob) {
		this.dayob = dayob;
		this.monthob = monthob;
		this.yearob = yearob;
		Calendar cal = Calendar.getInstance();
		cal.set(yearob, monthob-1, dayob);
		this.dateOfBirth = cal.getTime();
		this.localDateOfBirth = LocalDate.of(yearob, monthob, dayob);
		LocalDate now = LocalDate.now();
		this.age = Period.between(localDateOfBirth, now).getYears();
	}
	
	/**
	 * Getter methods for all the variables
	 * final ones should not be overridden as they are fully defined here
	 */
	public final String getFirstName() {
		return name.getFirstName();
	}

	public final String getLastName() {
		return name.getLastName();
	}

	public final int getAge() {
		return age;
	}
	
	public final int getDayob() {
		return dayob;
	}

	public final int getMonthob() {
		return monthob;
	}

	public final int getYearob() {
		return yearob;
	}

	public final Date getDateOfBirth() {
		return dateOfBirth;
	}
	
	public final LocalDate getLocalDateOfBirth() {
		return localDateOfBirth;
	}
	
	
	public final StudentID getStudentID() {
		
		return studentID;
	}

	
	public final SmartCard getSmartCard() {
		
		return smartCard;
	}
	
	
	public String toString() {
			
		return String.format("First Name: %s\nLast Name: %s\nAge: %s\nDate of Birth: %s\n%s"
				+ "\n%s\n----------------------------", 
				getFirstName(), getLastName(), age, dateOfBirth, smartCard.toString(), studentID.toString());
		
	}

}
