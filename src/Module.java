/**
 * Author: Mohamed Sayed
 * Date: 26th of April 2018
 * Purpose: Purpose is that it should store Module Objects 
 * 			will take its values from a text file
 *		
 */
public final class Module {
	
	/**
	 * A module consists of 3 properties:
	 * Name of module
	 * Code of module
	 * Amount of credits of module
	 */
	private String name;
	private String code;
	private String credits;
	
	/**
	 * Constructor is package-private
	 * It is only used within the package, therefore
	 * the only way to change a Module Object is by changing
	 * the content of the text file
	 * @param code
	 * @param name
	 * @param credits
	 */
	Module(String code, String name, String credits) {
		
		this.name = name;
		this.code = code;
		this.credits = credits;
	}
	
	/**
	 * Getter methods for name, code, amount of credits
	 * @return
	 */
	public final String getName() {
		return name;
	}

	public final String getCode() {
		return code;
	}

	public final String getCredits() {
		return credits;
	}
	
	/**
	 * toString() overridden
	 * presents the modules in a clear way
	 */
	public final String toString() {
		return String.format("%s %s %s", code, name, credits);
	}
	
	
}
