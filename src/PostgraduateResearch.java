import java.io.*;
import java.util.List;
import java.util.Scanner;


public final class PostgraduateResearch extends AbstractStudent{

/**
 * Author: Mohamed Sayed
 * Date: 26th of April 2018
 * Purpose: Creates a full Postgraduate Research Student
 * Using composition, only has one supervisor
 *
 */
	private Name supervisor;
	
/**
 * Extends the super() from the AbstractStudent class
 * has a specialized functionality of reading in the supervisor's name
 * 		from the text file
 * @param firstName
 * @param lastName
 * @param dayob
 * @param monthob
 * @param yearob
 */
	public PostgraduateResearch(String firstName, String lastName, int dayob, int monthob, int yearob) {
		
		super(firstName, lastName, dayob, monthob, yearob);
		
		try {
			Scanner s = new Scanner(
					new FileReader("/home/mo/Desktop/Textbooks/Programming ll/University Project/src/data3.txt"));
			
			this.supervisor = new Name(s.next(), s.next());
		}
		
		catch(Exception e) {
			System.err.println("Caught IO exception" + e.getMessage());
		}
	}
	
	/**
	 * A getter method for the supervisor
	 * @return
	 */
	
	public final Name getSupervisor() {
		return supervisor;
	}
	
	/**
	 * toString() overridden
	 * returns a full String presentation of the Postgraduate Research Student 
	 */
	public final String toString() {
		
		String stringRepresentation = super.toString() + "\n";
		
		stringRepresentation += "Supervisor: " + supervisor.getFirstName() + " " + supervisor.getLastName();
		stringRepresentation += "\n----------------------------\n"
				+ "----------------------------\n";
		
		return stringRepresentation;
	}

}
