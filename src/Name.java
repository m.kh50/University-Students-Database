/**
 * Author: Mohamed Sayed
 * Date: 26th of April 2018
 * Purpose: Name class is used to store any Name Object
 * 			Once has been initialized, does not change
 * 			String is an immutable object so does not
 * 			require making a copy of firstName & lastName
 *
 */

public final class Name {
	private final String firstName;
	private final String lastName;
	
	/**
	 * Assigns firstName and lastName a value specified in the parameters
	 * if the value is invalid or empty, throws an exception
	 * @param firstName
	 * @param lastName
	 */
	public Name(String firstName, String lastName) {
		if(firstName.length() > 0 && lastName.length() > 0) {
			this.firstName = firstName;
			this.lastName = lastName;
	
		}
		else throw new IllegalArgumentException("Name is invalid");
	}
	
	/**
	 * Getter methods for firstName & lastName fields
	 * @return
	 */
	public final String getFirstName() {
		return firstName;
	}
	
	public final String getLastName() {
		return lastName;
	}
}
