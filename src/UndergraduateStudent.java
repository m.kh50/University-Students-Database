import java.io.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * Author: Mohamed Sayed
 * Date: 26th of April 2018
 * Purpose: has specialized methods that just refer
 * 			to an Undergraduate Student & not any other object
 *
 */

public final class UndergraduateStudent extends AbstractStudent {
	
/**
 * list of modules
 * amountOfCredits will keep on adding modules till CREDITS_REQUIRED is met
 * PASS is the percentage required for student to pass every module 
 */
	private List<String> modules;
	private int amountOfCredits = 0;
	private final int PASS = 40;
	private final int CREDITS_REQUIRED = 120;
	
/**
 * Extends AbstractStudent's constructor
 * Adds to it a certain method which reads in
 * 		Undergraduate modules from a text file
 * @param firstName
 * @param lastName
 * @param dayob
 * @param monthob
 * @param yearob
 */
	public UndergraduateStudent(String firstName, String lastName, int dayob, int monthob, int yearob) {
		
		super(firstName, lastName, dayob, monthob, yearob);
		
		try {
			this.modules = setModules(new FileReader
					("/home/mo/Desktop/Textbooks/Programming ll/University Project/src/data1.txt"));
		}
		catch(FileNotFoundException e) {
			System.err.println("Caught IOException: " + e.getMessage());
		}
	}
	
	/**
	 * need to read in modules and store them into a list
	 * @param file which contains modules for Undergraduate Students
	 * @return returns a list of the modules
	 */
	private final List<String> setModules(FileReader file) {
		
		Scanner s = new Scanner(file);
		Scanner sc;
		modules = new ArrayList();
		
		while(s.hasNextLine() && amountOfCredits < CREDITS_REQUIRED) {
			String line = s.nextLine();
			sc = new Scanner(line);
			sc.useDelimiter(", ");
			String code = sc.next();
			String name = sc.next();
			String credits = sc.next();
			amountOfCredits += Integer.valueOf(credits);
			Module module = new Module(code, name, credits);
			modules.add(module.toString());
		}
		s.close();
		
		if(amountOfCredits != CREDITS_REQUIRED)
			throw new IllegalArgumentException("Credit requirements not met");
		
		Collections.unmodifiableList(modules);
		return modules;
	}
	
/**
 * Getter method for the list of Modules
 * @return
 */
	public final List<String> getModules() {
		return new ArrayList<String>(modules);
	}

/**
 * Override toString()
 * Returns the full String presentation of an Undergraduate Student
 */
	public final String toString() {
		String stringRepresentation = super.toString() + "\n";
		
		stringRepresentation += "Modules\n----------------------------\n";
		
		for(String m: modules) {
			stringRepresentation += m.toString() + "\n";
		}
		
		stringRepresentation += this.getFirstName() + " has to score at least " + PASS + "% to pass each module\n"; 
		stringRepresentation += "Total Amount of Credits: " + amountOfCredits; 
		stringRepresentation += "\n----------------------------\n"
				+ "----------------------------\n";
		
		return stringRepresentation;
	}	

}
