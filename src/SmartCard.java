import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;


public final class SmartCard {
	
/**
 * Author: Mohamed Sayed
 * Date: 26th of April 2018
 * Purpose: Allocates a unique Smart-Card for a Student Object
 *
 */
	
/**
 * Ingredients of the Smart Card Object
 */
	private final String firstName;
	private final String lastName;
	private final Date dateOfBirth;
	private final Date dateOfIssue;
	private String frstComponent;
	private final int year;
	private int serialNumber = 0;
	private String smartCardNo;
	private final String SMART_CARD_NO;
	
/**
 * keeps record of Smart Card to prevent duplicates
*/
	private static Map<String, IStudent> map = new HashMap<>();
 
/* @param takes in a Student Object
 * 		  uses the student's details for creating the smart card
 * 		  according to the University's rules 
 */
	<T extends IStudent> SmartCard(T student) {
		
		if(student instanceof UndergraduateStudent && student.getAge() >= 17
				|| (student instanceof PostgraduateTaught || student instanceof PostgraduateResearch 
						&& student.getAge() >= 20)) 
		{
		
			firstName = student.getFirstName();
			lastName = student.getLastName();
			dateOfIssue = Calendar.getInstance().getTime();
			dateOfBirth = student.getDateOfBirth();
			
				frstComponent = ("" + firstName.charAt(0) + lastName.charAt(0)).toUpperCase();
				
				
				year = Calendar.getInstance().get(Calendar.YEAR);
			
			do {
					if(serialNumber == 100) 
						throw new IllegalArgumentException("Ran out of serial "
								+ "numbers for current smart-card");
				if(serialNumber < 10) 
					smartCardNo = frstComponent + "-" + year + "-0" + serialNumber;
				
				else
					smartCardNo = frstComponent + "-" + year + "-" + serialNumber;
				
					serialNumber++;
			
			} while(map.containsKey(smartCardNo)); //loop again else add
			
			SMART_CARD_NO = new String(smartCardNo);
			
			map.put(SMART_CARD_NO, student);
		} 
		else {
			throw new IllegalArgumentException("Age requirement is not met");
		}
		
	}

/**
 * Getter methods of fields 
 * 
 */
	public final Date getDateOfIssue() {
		return (Date) dateOfIssue.clone();
	}
	

	public final String getFirstName() {
		return firstName;
	}

	public final String getLastName() {
		return lastName;
	}

	public final Date getDateOfBirth() {
		return (Date) dateOfBirth.clone();
	}

	public final String getSMART_CARD_NO() {
		return SMART_CARD_NO;
	}

	public final String getFrstComponent() {
		
		return frstComponent;
	}

	public final int getYear() {
		return year;
	}

	public final int getSerialNumber() {
		return serialNumber;
	}
	
	static final Map<String, IStudent> getMap() {
		return map;
	}

/**
 * Removes Smart-Card from map and deletes Student Object from the records
 * 		in case of terminating a Student
 * @param smartCard
 */
	final void removeStudent() {
		String smartCard = this.getSMART_CARD_NO();
		map.remove(smartCard);
	}
	
/**
 * toString() overridden
 * Returns an elegant and a clear presentation of the Smart-Card Object
 */
	public final String toString() {
		return String.format
				("----------------------------\n"
						+ "SMART CARD DETAILS\n----------------------------"
						+ "\nStudent Name: %s %s\nDate of Birth: %s\nSmart Card Number: %s\n"
						+ "Date of Issue: %s",
						firstName, lastName, dateOfBirth, SMART_CARD_NO, dateOfIssue
				);
	}

/**
 * hashCode() overridden
 */
	@Override
	public final int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((SMART_CARD_NO == null) ? 0 : SMART_CARD_NO.hashCode());
		result = prime * result + ((dateOfBirth == null) ? 0 : dateOfBirth.hashCode());
		result = prime * result + ((dateOfIssue == null) ? 0 : dateOfIssue.hashCode());
		result = prime * result + ((firstName == null) ? 0 : firstName.hashCode());
		result = prime * result + ((lastName == null) ? 0 : lastName.hashCode());
		return result;
	}

/**
 * equals() overriden
 */
	@Override
	public final boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (!(obj instanceof SmartCard)) {
			return false;
		}
		SmartCard other = (SmartCard) obj;
		if (SMART_CARD_NO == null) {
			if (other.SMART_CARD_NO != null) {
				return false;
			}
		} else if (!SMART_CARD_NO.equals(other.SMART_CARD_NO)) {
			return false;
		}
		if (dateOfBirth == null) {
			if (other.dateOfBirth != null) {
				return false;
			}
		} else if (!dateOfBirth.equals(other.dateOfBirth)) {
			return false;
		}
		if (dateOfIssue == null) {
			if (other.dateOfIssue != null) {
				return false;
			}
		} else if (!dateOfIssue.equals(other.dateOfIssue)) {
			return false;
		}
		if (firstName == null) {
			if (other.firstName != null) {
				return false;
			}
		} else if (!firstName.equals(other.firstName)) {
			return false;
		}
		if (lastName == null) {
			if (other.lastName != null) {
				return false;
			}
		} else if (!lastName.equals(other.lastName)) {
			return false;
		}
		return true;
	}
	
	
}
